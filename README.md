# MessageKrowd

MessageKrowd is a communication plugin providing core chat features.
It automatically fetches the prefix and suffix via Vault. It also integrates
with BanManager to check if the player is muted. If the server has ProtocolLib
then it can also detect scoreboard-teams prefix/suffixes.

### [Bugs & Feature Requests](https://gitlab.com/cubekrowd/messagekrowd/issues "Bugs & Feature Requests")

## Features

* Ability to group servers into groups sharing chat
* Integrates with Vault to fetch prefix/suffix
* Private messaging with /m, /msg, /w or /message
* Smart-reply with /r or /reply
* Away-from-keyboard functionality with /afk
* Custom styles for private messages
* Permission for use of chat colour codes
* Censor filter to prevent bad words
* Duplicate-message, caps-limit and rate-limit spam protection
* Socialspy for moderators with /socialspy
* Ability to silence per chatgroup or globally
* Optional: Checks if the player is muted in BanManager
* Optional: Fetches the scoreboard-teams prefix/suffix with ProtocolLib
* Optional: AFK support with BungeeTabListPlus

## Installation

Drop the plugin into the plugin folder on all servers including BungeeCord and restart.
Remember to edit the config on the bungeecord server. It is required to restart
for the config to update.

## Permissions

* messagekrowd.formatcodes - gives full access to use of chat formatting codes
* messagekrowd.formatcodeslimited - gives limited access to use of chat formatting codes (&m &n &o &r)
* messagekrowd.socialspy - gives access to /socialspy
* messagekrowd.admin - gives access to /messagekrowd utility/debug command
* messagekrowd.afk - gives access to go afk with the /afk command
* messagekrowd.silence - gives access to /silence
* messagekrowd.silencebypass - gives permission to bypass the silence restriction

## Credits

This plugin is Open-Source, released under AGPL-3.0.
If you want to contribute you can find the repository at:  
[https://gitlab.com/cubekrowd/messagekrowd](https://gitlab.com/cubekrowd/messagekrowd "https://gitlab.com/cubekrowd/messagekrowd")