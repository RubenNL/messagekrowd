package net.cubekrowd.messagekrowd.bungeecord.command;

import java.util.stream.Collectors;
import net.cubekrowd.messagekrowd.bungeecord.MessageKrowdBungeeCordPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class AFKCommand extends Command {

    private final MessageKrowdBungeeCordPlugin plugin;

    public AFKCommand(MessageKrowdBungeeCordPlugin plugin) {
        super("afk", "messagekrowd.afk");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            if (!(sender instanceof ProxiedPlayer)) {
                sender.sendMessage(ChatColor.RED + "This is a player-only command");
                return;
            }
            ProxiedPlayer pp = ((ProxiedPlayer) sender);

            // Rate-limit
            var settings = plugin.globalSettings;
            var onlineData = plugin.getOnlineData(pp);

            var newPoints = onlineData.rateLimitPoints.addAndGet(settings.rateLimitPointsPerMessage);
            var serverName = pp.getServer().getInfo().getName();

            if (newPoints > settings.rateLimitThreshold
                    && (settings.spamProtectedServers.isEmpty()
                    || settings.spamProtectedServers.contains(serverName))) {
                pp.sendMessage(ChatColor.translateAlternateColorCodes('&', settings.rateLimitMessage));
                return;
            }

            onlineData.toggleAFK(plugin, pp);
            return;
        }

        // Arguments are more than 0
        if (args[0].equalsIgnoreCase("list")) {
            String afkList = plugin.getProxy().getPlayers().stream()
                    .filter(plugin::isAFK)
                    .map(CommandSender::getName)
                    .collect(Collectors.joining(", "));
            sender.sendMessage(plugin.prefix + ChatColor.DARK_AQUA
                    + "People AFK: " + ChatColor.AQUA + afkList);
            return;
        }

        sender.sendMessage(plugin.prefix
                + ChatColor.RED + "Invalid command! Please do "
                + ChatColor.WHITE + "/afk" + ChatColor.RED + " or "
                + ChatColor.WHITE + "/afk list" + ChatColor.RED + " instead.");
    }
}
