package net.cubekrowd.messagekrowd.bungeecord;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.*;
import codecrafter47.bungeetablistplus.api.bungee.*;

public class AFKVariable extends Variable {

    private final MessageKrowdBungeeCordPlugin plugin;

    public AFKVariable(MessageKrowdBungeeCordPlugin plugin) {
        super("messagekrowd_afk");
        this.plugin = plugin;
    }

    @Override
    public String getReplacement(ProxiedPlayer pp) {
        // @NOTE(traks) Previously (v2) we used BungeeTabListPlus's
        // customPlaceholders to create the following placeholder:
        //
        //  afk_tag:
        //     !conditional
        //     condition: ${player messagekrowd_afk}
        //     'false': ''
        //     'true': '&5*'
        //
        // and we used that in the playerComponent to display the AFK *.
        // However, in v3 this no longer seems to work (this variable doesn't
        // get called), so instead we just directly return the * here. We can
        // simply use ${player messagekrowd_afk} directly in the playerComponent
        // then and everything works fine.

        if (plugin.isAFK(pp)) {
            return ChatColor.DARK_PURPLE + "*";
        } else {
            return "";
        }
    }
}
