package net.cubekrowd.messagekrowd.bungeecord.command;

import java.util.stream.Collectors;
import net.cubekrowd.messagekrowd.bungeecord.MessageKrowdBungeeCordPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class MessageKrowdCommand extends Command {

    private final MessageKrowdBungeeCordPlugin plugin;

    public MessageKrowdCommand(MessageKrowdBungeeCordPlugin plugin) {
        super("messagekrowd", null);
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        var settings = plugin.globalSettings;

        if (args.length == 0) {
            sender.sendMessage(plugin.prefix + ChatColor.GREEN + "Running version v" +
                               plugin.getDescription().getVersion());
            if (sender.hasPermission("messagekrowd.admin")) {
                sender.sendMessage(plugin.prefix + ChatColor.DARK_AQUA + "/messagekrowd " +
                                   ChatColor.AQUA + "reload");
            }
            if (sender.hasPermission("messagekrowd.silence")) {
                sender.sendMessage(plugin.prefix + ChatColor.DARK_AQUA + "/messagekrowd " +
                                   ChatColor.AQUA + "listtargets");
            }
            if (sender.hasPermission("messagekrowd.silence")) {
                sender.sendMessage(plugin.prefix + ChatColor.DARK_AQUA + "/messagekrowd " +
                                   ChatColor.AQUA + "listsilence");
            }
            return;
        }

        if (args[0].equalsIgnoreCase("reload")) {
            if (!sender.hasPermission("messagekrowd.admin")) {
                sender.sendMessage(ChatColor.RED +
                                   "You do not have permission to execute this command!");
                return;
            }

            if (plugin.reloadFromConfig()) {
                sender.sendMessage(plugin.prefix + ChatColor.DARK_AQUA + "Reloaded the config!");
            } else {
                sender.sendMessage(plugin.prefix + ChatColor.RED + "Failed to reload the config!");
            }
            return;
        }

        if (args[0].equalsIgnoreCase("listtargets")) {
            if (!sender.hasPermission("messagekrowd.silence")) {
                sender.sendMessage(ChatColor.RED +
                                   "You do not have permission to execute this command!");
                return;
            }

            sender.sendMessage(plugin.prefix + ChatColor.DARK_AQUA + "Global targets: " + ChatColor.AQUA + "all");
            sender.sendMessage(plugin.prefix + ChatColor.DARK_AQUA + "Group targets: " +
                    ChatColor.AQUA + settings.serverGroups.stream().map(g -> g.name).collect(Collectors.joining(", ")));
            return;
        }

        if (args[0].equalsIgnoreCase("listsilence")) {
            if (!sender.hasPermission("messagekrowd.silence")) {
                sender.sendMessage(ChatColor.RED +
                                   "You do not have permission to execute this command!");
                return;
            }

            synchronized (plugin.globalLock) {
                var silenced = settings.serverGroups.stream()
                        .map(g -> g.name)
                        .filter(plugin.silencedGroups::contains)
                        .collect(Collectors.joining(", "));
                sender.sendMessage(plugin.prefix + ChatColor.DARK_AQUA
                        + "Silenced targets: " + ChatColor.AQUA + silenced);
                return;
            }
        }

        sender.sendMessage(plugin.prefix + ChatColor.RED + "Invalid command! Please do " + ChatColor.WHITE + "/messagekrowd" + ChatColor.RED + " for more information.");
    }
}
