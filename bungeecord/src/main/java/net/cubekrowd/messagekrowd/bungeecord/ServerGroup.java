package net.cubekrowd.messagekrowd.bungeecord;

import java.util.ArrayList;
import java.util.List;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;

public class ServerGroup {
    public String name;
    public List<ServerInfo> servers;

    public ServerGroup(String name) {
        this.name = name;
    }

    public void setServersWithNames(List<String> serverNames) {
        this.servers = new ArrayList<>(serverNames.size());
        for (var serverInfo : ProxyServer.getInstance().getServers().values()) {
            if (serverNames.contains(serverInfo.getName())) {
                servers.add(serverInfo);
            }
        }
    }
}
