package net.cubekrowd.messagekrowd.bungeecord.command;

import net.cubekrowd.messagekrowd.bungeecord.MessageKrowdBungeeCordPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class SilenceCommand extends Command {

    private final MessageKrowdBungeeCordPlugin plugin;

    public SilenceCommand(MessageKrowdBungeeCordPlugin plugin) {
        super("silence", "messagekrowd.silence");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!((args.length == 1 && args[0].equalsIgnoreCase("all"))
                || args.length == 2)) {
            sender.sendMessage(plugin.prefix + ChatColor.RED + "Usage: /silence all");
            sender.sendMessage(plugin.prefix + ChatColor.RED + "Usage: /silence group <chatgroup>");
            return;
        }

        var settings = plugin.globalSettings;
        String selector = args[0].toLowerCase();

        if (selector.equals("all")) {
            synchronized (plugin.silencedGroups) {
                if (settings.serverGroups.size() == plugin.silencedGroups.size()) {
                    // all groups silenced, so unmute
                    plugin.silencedGroups.clear();
                    sender.sendMessage(plugin.prefix + ChatColor.DARK_AQUA
                            + "All servers are now unmuted.");
                } else {
                    settings.serverGroups.forEach(g -> plugin.silencedGroups.add(g.name));
                    sender.sendMessage(plugin.prefix + ChatColor.DARK_AQUA
                            + "All servers are now muted.");
                }
            }
            return;
        }

        String target = args[1].toLowerCase();

        if (selector.equals("group")) {
            // Find chat group
            for (var group : settings.serverGroups) {
                if (group.name.equalsIgnoreCase(target)) {
                    synchronized (plugin.globalLock) {
                        boolean muted;
                        if (plugin.silencedGroups.contains(group.name)) {
                            plugin.silencedGroups.remove(group.name);
                            muted = false;
                        } else {
                            plugin.silencedGroups.add(group.name);
                            muted = true;
                        }
                        sender.sendMessage(plugin.prefix + ChatColor.DARK_AQUA
                                + "All servers in the " + ChatColor.GRAY + target
                                + ChatColor.DARK_AQUA + " chat group are now "
                                + (muted ? "" : "un") + "muted.");
                    }
                    return;
                }
            }
        }

        sender.sendMessage(plugin.prefix + ChatColor.RED + "Target not found! Do " +
                           ChatColor.DARK_AQUA + "/messagekrowd listtargets" + ChatColor.RED +
                           " to list possible targets.");
    }
}
