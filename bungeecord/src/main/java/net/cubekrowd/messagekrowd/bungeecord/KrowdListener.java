package net.cubekrowd.messagekrowd.bungeecord;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import net.cubekrowd.eventstorageapi.api.EventEntry;
import net.cubekrowd.eventstorageapi.api.EventStorageAPI;
import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.query.QueryOptions;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.TranslatableComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.TabCompleteEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

public class KrowdListener implements Listener {

    private final MessageKrowdBungeeCordPlugin plugin;

    public KrowdListener(MessageKrowdBungeeCordPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPostLogin(PostLoginEvent e) {
        // @NOTE(traks) client connected to BungeeCord and BungeeCord just
        // switched protocol state to PLAY and just sent login success packet.

        var pp = e.getPlayer();

        // Remove social-spy if they are no longer staff
        if (!pp.hasPermission("messagekrowd.socialspy")) {
            if (plugin.allSocialSpies.remove(pp.getUniqueId()) != null) {
                plugin.saveSocialSpies();
                e.getPlayer().sendMessage(plugin.prefix + ChatColor.DARK_AQUA
                        + "Turned socialspy " + ChatColor.RED + "off");
                plugin.getLogger().info("Turned socialspy off for " + pp.getName() + " because they don't have permission");
            }
        }
    }

    @EventHandler
    public void onPlayerDisconnect(PlayerDisconnectEvent e) {
        // @NOTE(traks) fired if player disconnects after postlogin happened

        var uuid = e.getPlayer().getUniqueId();
        plugin.onlineDataMap.remove(uuid);
    }

    @EventHandler
    public void onTabComplete(TabCompleteEvent e) {
        // Return if they are doing an unknown command
        List<String> aliases = Arrays.asList("message", "m", "msg", "w", "whisper", "tell", "reply", "r", "find");
        if (e.getCursor().startsWith("/") && !aliases.contains(e.getCursor().toLowerCase().split(" ")[0].substring(1))) {
            return;
        }

        int p = e.getCursor().lastIndexOf(" ");
        String lastArg = e.getCursor().substring(p + 1);
        String lowerArg = lastArg.toLowerCase(Locale.ENGLISH);

        plugin.getProxy().getPlayers().stream()
                .map(ProxiedPlayer::getName)
                .filter(u -> u.toLowerCase(Locale.ENGLISH).startsWith(lowerArg))
                .filter(u -> !e.getSuggestions().contains(u))
                .forEach(e.getSuggestions()::add);
    }

    // @NOTE(traks) This is org.apache.commons.lang3.StringUtils.normalizeSpace,
    // but slightly modified. See their website for licencing details.
    public static String normalizeSpace(String str, boolean keepNonBreakingSpaces) {
        if (str == null || str.length() == 0) {
            return str;
        } else {
            int size = str.length();
            char[] newChars = new char[size];
            int count = 0;
            int whitespacesCount = 0;
            boolean startWhitespaces = true;

            for(int i = 0; i < size; ++i) {
                char actualChar = str.charAt(i);
                boolean isWhitespace = Character.isWhitespace(actualChar);
                if (!isWhitespace) {
                    startWhitespaces = false;
                    newChars[count++] = actualChar == 160 && !keepNonBreakingSpaces ? 32 : actualChar;
                    whitespacesCount = 0;
                } else {
                    if (whitespacesCount == 0 && !startWhitespaces) {
                        newChars[count++] = " ".charAt(0);
                    }

                    ++whitespacesCount;
                }
            }

            if (startWhitespaces) {
                return "";
            } else {
                return (new String(newChars, 0, count - (whitespacesCount > 0 ? 1 : 0))).trim();
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onChat(ChatEvent e) {
        // Do not process cancelled events
        if (e.isCancelled()) {
            return;
        }
        if (!(e.getSender() instanceof ProxiedPlayer pp)) {
            return;
        }

        {
            // @NOTE(traks) BungeeCord already does this, but only since
            // recently. Might as well keep it to stay on the safe side. Who
            // knows what BungeeCord decides to change...
            var msg = e.getMessage();
            for (int i = 0; i < msg.length(); i++) {
                char c = msg.charAt(i);
                if (c == '§' || c < ' ' || c == 127) {
                    // @NOTE(traks) illegal character, disconnect
                    pp.disconnect(new TranslatableComponent("multiplayer.disconnect.illegal_characters"));
                    e.setCancelled(true);
                    return;
                }
            }
        }

        var ppServer = pp.getServer().getInfo();
        var settings = plugin.globalSettings;
        var ppServerGroup = settings.getServerGroupByServer(ppServer);
        var ppServerName = ppServer.getName();

        var passthrough = settings.passthroughServers.contains(ppServerName) || e.isCommand();

        var onlineData = plugin.getOnlineData(pp);

        if (!e.getMessage().toLowerCase().startsWith("/afk")) {
            onlineData.markSeen(plugin, pp);
        }

        // @NOTE(traks) MC's chat packet handler normalises space. We do the
        // same, since BungeeCord doesn't do it for us.
        //
        // Also used for commands! This fixes the issue where e.g.
        // "/m  username hi" sends a seemingly random player (i.e. the player
        // whose name best matches an empty string) the message "username hi",
        // instead of sending "hi" to the player named "username".
        //
        // Be careful when normalising space for non-proxy commands: these are
        // forwarded to the back-end server. Since space normalisation replaces
        // non-breaking spaces with spaces, if we normalise here and forward to
        // the back-end server, it will normalise too and get rid of the spaces
        // that were non-breaking spaces.
        //
        // Similarly for chat we pass through.

        // Do not process commands unless they are in the list
        if (e.isCommand()) {
            if (e.isProxyCommand()) {
                e.setMessage(normalizeSpace(e.getMessage(), false));
            } else {
                e.setMessage(normalizeSpace(e.getMessage(), true));
            }

            // Log commands
            if (plugin.hasESAPI) {
                Map<String, String> data = new HashMap<>();
                data.put("u", pp.getUniqueId().toString());
                data.put("s", pp.getServer().getInfo().getName());
                data.put("m", e.getMessage());
                EventStorageAPI.getStorage().addEntry(new EventEntry(plugin.getDescription().getName(), "command", data));
            }

            // Check list of commands to process (these should be chat or message-related)
            var processCommand = false;
            for (var cmd : settings.commandsToProcess) {
                var msg = e.getMessage().toLowerCase(Locale.ENGLISH);
                var prefix = "/" + cmd.toLowerCase(Locale.ENGLISH);
                if (msg.startsWith(prefix + " ") || msg.startsWith(prefix + ((char) 160))) {
                    processCommand = true;
                    break;
                }
            }

            if (!processCommand) {
                return;
            }
        } else {
            if (!passthrough) {
                e.setMessage(normalizeSpace(e.getMessage(), false));
            } else {
                e.setMessage(normalizeSpace(e.getMessage(), true));
            }
        }

        // Cancel chat event if we don't want passthrough
        if (!passthrough) {
            e.setCancelled(true);
        }

        // Replace fullwidth characters and other characters commonly used
        // for spam and bypassing swear filter
        if (settings.replaceFullwidth) {
            char[] charMessage = e.getMessage().toCharArray();
            for (int i = 0; i < charMessage.length; i++) {
                if (charMessage[i] >= '\uFF00' && charMessage[i] < '\uFF60') {
                    // Fullwidth characters
                    charMessage[i] -= '\uFF00' - ' ';
                } else if (charMessage[i] >= '\u249C' && charMessage[i] < '\u24B6') {
                    // Enclosed alphanumerics, letters of the form (x)
                    charMessage[i] -= '\u249C' - 'a';
                } else if (charMessage[i] >= '\u24B6' && charMessage[i] < '\u24D0') {
                    // Enclosed alphanumerics, circled capital letters
                    charMessage[i] -= '\u24B6' - 'A';
                } else if (charMessage[i] >= '\u24D0' && charMessage[i] < '\u24EA') {
                    // Enclosed alphanumerics, circled lowercase letters
                    charMessage[i] -= '\u24D0' - 'a';
                }
            }
            e.setMessage(new String(charMessage));
        }

        //
        // SPAM Protection
        //
        // Check caps-limit
        String workm = e.getMessage();
        // remove all usernames
        for (var onlinePlayer : plugin.getProxy().getPlayers()) {
            workm = workm.replaceAll(onlinePlayer.getName().toUpperCase(), "");
        }

        var spamProtectedServers = settings.spamProtectedServers;
        if (workm.length() > 3 && (spamProtectedServers.size() == 0
                || spamProtectedServers.contains(ppServerName))) {
            double uppercase = workm.chars().mapToObj(i -> (char) i).filter(Character::isUpperCase).count();
            if (100 * (uppercase / workm.length()) >= settings.capsLimit) {
                pp.sendMessage(ChatColor.translateAlternateColorCodes('&', settings.capsMessage));
                return;
            }
        }

        // Rate-limit
        var newPoints = onlineData.rateLimitPoints.addAndGet(settings.rateLimitPointsPerMessage);
        if (newPoints > settings.rateLimitThreshold && (spamProtectedServers.isEmpty()
                || spamProtectedServers.contains(ppServerName))) {
            pp.sendMessage(ChatColor.translateAlternateColorCodes('&', settings.rateLimitMessage));
            return;
        }

        //
        // Censor filter
        //
        if (settings.censorEnabled && (settings.censoredServers.isEmpty()
                || settings.censoredServers.contains(ppServerName))) {
            for (String word : settings.censoredWords) {
                if (settings.censorBlock) {
                    if (e.getMessage().toLowerCase().contains(word)) {
                        pp.sendMessage(ChatColor.translateAlternateColorCodes('&', settings.censorMessage));
                        return;
                    }
                } else {
                    var stars = "*".repeat(word.length());
                    e.setMessage(e.getMessage().replaceAll("(?i)" + word, stars));
                }
            }
        }

        String display = onlineData.getServerDisplayName(pp);

        if (AprilFools.isAprilFools()) display = AprilFools.reverse(display);
        if (plugin.hasLuckPerms && !settings.teamPrefixSuffixServers.contains(ppServerName)) {
            var api = LuckPermsProvider.get();
            var user = api.getUserManager().getUser(pp.getUniqueId());
            if (user != null) {
                var context = api.getContextManager().getContext(user);
                if (context.isPresent()) {
                    var metaData = user.getCachedData().getMetaData(QueryOptions.contextual(context.get()));
                    if (AprilFools.isAprilFools()) display = AprilFools.reverse(pp.getDisplayName());
                    display = (metaData.getPrefix() != null ? metaData.getPrefix() : "") +
                              display + (metaData.getSuffix() != null ? metaData.getPrefix() : "");
                    display = ChatColor.translateAlternateColorCodes('&', display);
                }
            }

        }

        // Use formatting codes in chat
        // Bukkit does not allow § in chat, so pass through unprocessed text
        // in that case.
        if (pp.hasPermission("messagekrowd.formatcodes") && !passthrough) {
            e.setMessage(ChatColor.translateAlternateColorCodes('&',
                         e.getMessage()).replaceAll(ChatColor.MAGIC + "", ""));
        }
        if (pp.hasPermission("messagekrowd.formatcodeslimited") && !passthrough) {
            e.setMessage(e.getMessage().replaceAll("&m", "" + ChatColor.STRIKETHROUGH));
            e.setMessage(e.getMessage().replaceAll("&n", "" + ChatColor.UNDERLINE));
            e.setMessage(e.getMessage().replaceAll("&o", "" + ChatColor.ITALIC));
            e.setMessage(e.getMessage().replaceAll("&r", "" + ChatColor.RESET));
        }

        // Prepend white before message
        if (!passthrough) {
            e.setMessage(ChatColor.WHITE + e.getMessage());
        }

        long time = System.currentTimeMillis();

        synchronized (plugin.globalLock) {
            // @NOTE(traks) need to do all this stuff so messages for sure don't
            // appear after a chat group has been silenced (and the staff member
            // received the message confirming the silence)

            // Check if silence is in effect
            if (plugin.silencedGroups.contains(ppServerGroup.name)
                    && !pp.hasPermission("messagekrowd.silencebypass")) {
                pp.sendMessage(ChatColor.translateAlternateColorCodes('&', settings.silenceFormat));
                return;
            }

            if (ppServerGroup.servers.size() > 1) {
                logChat(pp, "g_" + ppServerGroup.name, display, e.getMessage(), time);

                // If this message is to be passed through to the bukkit server,
                // do not send it to all players or the messagekrowd bukkit plugin
                // (to avoid duplicate messages arriving in chat)
                if (passthrough) {
                    return;
                }

                for (var si : ppServerGroup.servers) {
                    var msg = TextComponent.fromLegacyText(display + " " + e.getMessage());
                    si.getPlayers().forEach(sp -> sp.sendMessage(msg));
                    // temporarily send it to bukkit side for logging, this is only printed to console
                    plugin.sendToBukkit(si, "Chat", time + " " + display + " " + e.getMessage());
                }
            } else {
                // search for a group did not return the function which means no result
                // add brackets because of no group to immitate vanilla behaviour
                display = ChatColor.WHITE + "<" + display + ChatColor.WHITE + ">";
                logChat(pp, "s_" + ppServer.getName(), display, e.getMessage(), time);

                // If this message is to be passed through to the bukkit server,
                // do not send it to all players or the messagekrowd bukkit plugin
                // (to avoid duplicate messages arriving in chat)
                if (passthrough) {
                    return;
                }

                var msg = TextComponent.fromLegacyText(display + " " + e.getMessage());
                ppServer.getPlayers().forEach(sp -> sp.sendMessage(msg));
                // temporarily send it to bukkit side for logging, this is only printed to console
                plugin.sendToBukkit(ppServer, "Chat", time + " " + display + " " + e.getMessage());
            }
        }
    }

    private void logChat(ProxiedPlayer pp, String target, String display, String message, long time) {
        if (plugin.hasESAPI) {
            Map<String, String> data = new HashMap<>();
            data.put("u", pp.getUniqueId().toString());
            data.put("t", target);
            data.put("d", display);
            data.put("m", message);
            EventStorageAPI.getStorage().addEntry(new EventEntry(plugin.getDescription().getName(), "chat", time, data));
        }
    }

    @EventHandler
    public void onPluginMessage(PluginMessageEvent e) {
        try {
            if (e.getTag().equals(plugin.getMessageChannelName())) {
                e.setCancelled(true);

                if (!(e.getSender() instanceof Server)) {
                    return;
                }
                ServerInfo server = ((Server) e.getSender()).getInfo();
                ByteArrayInputStream b = new ByteArrayInputStream(e.getData());
                DataInputStream in = new DataInputStream(b);
                String subChannel = in.readUTF();

                var settings = plugin.globalSettings;

                switch (subChannel) {
                case "DisplayTeam":
                    if (settings.teamPrefixSuffixServers.contains(server.getName())) {
                        var uuid = UUID.fromString(in.readUTF());
                        var newDisplayName = in.readUTF();
                        var pp = plugin.getProxy().getPlayer(uuid);
                        if (pp != null) {
                            var onlineData = plugin.getOnlineData(pp);
                            onlineData.displayNameByServer.put(server.getName(), newDisplayName);
                        }
                    }
                    break;
                case "Move": {
                    var uuid = UUID.fromString(in.readUTF());
                    var pp = plugin.getProxy().getPlayer(uuid);
                    if (pp != null) {
                        var onlineData = plugin.getOnlineData(pp);
                        onlineData.markSeen(plugin, pp);
                    }
                    break;
                }
                }
            }
        } catch (Exception ex) {
            plugin.getLogger().log(Level.SEVERE, "Failed to handle plugin message", ex);
        }
    }
}
