package net.cubekrowd.messagekrowd.bukkit;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

public class KrowdPluginMessageListener implements PluginMessageListener {

    private final MessageKrowdBukkitPlugin plugin;

    public KrowdPluginMessageListener(MessageKrowdBukkitPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player,
            byte[] message) {
        DataInputStream in = new DataInputStream(new ByteArrayInputStream(message));
        String subChannel = null;
        try {
            subChannel = in.readUTF();

            if (subChannel.equals("Chat")) {
                ConsoleCommandSender console = plugin.getServer().getConsoleSender();
                console.sendMessage(in.readUTF());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
