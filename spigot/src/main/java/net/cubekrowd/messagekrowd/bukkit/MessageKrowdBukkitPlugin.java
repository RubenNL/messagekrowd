package net.cubekrowd.messagekrowd.bukkit;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.entity.minecart.RideableMinecart;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Team;

public class MessageKrowdBukkitPlugin extends JavaPlugin implements Listener {
    // Since 1.13, plugin message channel names must be lower case and must be
    // namespaced.
    private static final String MSG_CHANNEL = "messagekrowd:generic";
    private final Map<Player, Location> locations = new HashMap<>();

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
        getServer().getMessenger().registerOutgoingPluginChannel(this, MSG_CHANNEL);
        getServer().getMessenger().registerIncomingPluginChannel(this, MSG_CHANNEL,
                new KrowdPluginMessageListener(this));

        getServer().getScheduler().runTaskTimer(this, () -> {
            for (Player p : getServer().getOnlinePlayers()) {
                var oldLoc = locations.put(p, p.getLocation());
                if (oldLoc == null) {
                    continue;
                }

                if (!oldLoc.isWorldLoaded()) {
                    continue;
                }

                // Do not count gliding as movement
                if (p.isGliding()) {
                    continue;
                }
                // Do not un-afk if riding minecart
                if (p.getVehicle() != null && p.getVehicle() instanceof RideableMinecart) {
                    continue;
                }

                // Check if different worlds
                if (!oldLoc.getWorld().equals(p.getLocation().getWorld())) {
                    sendPluginMessage(p, "Move", false, p.getUniqueId().toString());
                    continue;
                }
                if (oldLoc.distanceSquared(p.getLocation()) > 3) {
                    sendPluginMessage(p, "Move", false, p.getUniqueId().toString());
                }
            }
        }, 20, 20);
    }

    @Override
    public void onDisable() {
        locations.clear();
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoin(PlayerJoinEvent e) {
        updateDisplayName(e.getPlayer());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit(PlayerQuitEvent e) {
        locations.remove(e.getPlayer());
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
    public void onPlayerTeleport(PlayerTeleportEvent e) {
        // For MissileWars. The team of a player is set when they teleport.
        updateDisplayName(e.getPlayer());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onGamemodeChange(PlayerGameModeChangeEvent e) {
        // Detect spectator change in MW
        updateDisplayName(e.getPlayer());
    }

    public void updateDisplayName(Player player) {
        // Catch TemporaryPlayers of ProtocolLib. See the code of ProtocolLib
        // for more information.
        try {
            player.getWorld();
        } catch (Exception e) {
            return;
        }

        player.getScoreboard();
        String display = player.getName();
        Team team = player.getScoreboard().getEntryTeam(player.getName());
        if(team != null) {
            display = team.getPrefix() + team.getColor() + player.getName() + team.getSuffix();
        }
        sendPluginMessage(player, "DisplayTeam", true, player.getUniqueId().toString(),
                ChatColor.translateAlternateColorCodes('&', display));
    }

    public void sendPluginMessage(Player player, String subChannel, boolean delay,
                                  String... data) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF(subChannel);
        Stream.of(data).forEach(out::writeUTF);
        getServer().getScheduler().runTaskLater(this, () -> {
            if (player.isOnline()) {
                player.sendPluginMessage(MessageKrowdBukkitPlugin.this, MSG_CHANNEL,
                        out.toByteArray());
            }
        }, delay ? 2 : 0);
        if (delay) {
            getServer().getScheduler().runTaskLater(this, () -> {
                if (player.isOnline()) {
                    player.sendPluginMessage(MessageKrowdBukkitPlugin.this, MSG_CHANNEL,
                            out.toByteArray());
                }
            }, 10);
        }
    }
}
